#include <stdio.h>
#include <X11/Xlib.h>
#include <X11/xpm.h>


int main ( int args_count, char ** args)
{
  printf("%d %s\n", args_count, args[2]);

  Display * dis;
  int root, screen, win, black, white;
  GC gc;

  dis = XOpenDisplay (0);
  screen = DefaultScreen (dis);
  root = RootWindow (dis, screen);
  black = BlackPixel (dis, screen);
  white = WhitePixel (dis, screen);

  win = XCreateSimpleWindow (dis, root, 0,0, 300,300, 0,0,0);
  XMapWindow (dis, win);
  XSync (dis, 1);
  getchar ();
}
