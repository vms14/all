#include <sys/socket.h>
#include <arpa/inet.h>
#include <strings.h>
#include <err.h>
#include <stdlib.h>

typedef struct
{
    int fd;
    struct sockaddr_in info;
} sock;

sock tcp_socket (const char * ip_address, uint32_t port)
{
    sock new_socket;
    socklen_t info_len;

    info_len = sizeof (new_socket.info);
    
    new_socket.fd = socket (AF_INET, SOCK_STREAM, 0);

    if (new_socket.fd < 0)
    {
	warn ("Socket creation failed");
    }

    bzero (&new_socket.info, sizeof(new_socket.info));

    new_socket.info.sin_family = AF_INET;
    new_socket.info.sin_port = htons (port);


    if (inet_pton (AF_INET, ip_address, &new_socket.info.sin_addr) <= 0)
    {
	warn ("Failed while binding ip address");
    }

    if ((connect (new_socket.fd, (struct sockaddr *) &new_socket.info, info_len)) < 0)
    {
	warn ("Failed while connecting socket");
    }

    return new_socket;
}
