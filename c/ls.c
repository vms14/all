#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/un.h>

int main (int count, char ** args)
{

  int fd, current_client;
  fd = socket(AF_UNIX, SOCK_STREAM, 0);
  

  struct sockaddr_un addr, client;
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, "/home/vms/ohmysocket", sizeof(addr.sun_path)-1);
  
  unlink("/home/vms/ohmysocket");
  
  printf("bind %d \n",bind(fd, (struct sockaddr*)&addr, sizeof(addr)));
  printf("listen %d \n",listen(fd, 3));
  
  socklen_t len;
  len = sizeof(client);
  
  while (1)
  {
    current_client = accept(fd, (struct sockaddr *)&client, &len);
    write (current_client, "oh my cat", sizeof("oh my cat"));
    write (current_client, "\n", 1);
    close (current_client);
  }

}
