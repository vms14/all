canvas = document.body.appendChild(document.createElement("canvas"));
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

ctx = canvas.getContext("2d");

seconds = 0;
minutes = 0;
hours = 0;

function start ()
{
    interval = setInterval(()=>
    {
	seconds++;
	check();
	draw();
    }, 1000);
}

function stop ()
{
    clearInterval(interval);
}

function reset ()
{
    stop();
    seconds = 0;
    minutes = 0;
    hours = 0;
    start();
}

function check ()
{
    if (seconds >= 60)
    {
	seconds = 0;
	minutes++;
    }
    if (minutes >= 60)
    {
	minutes = 0;
	hours++;
    }

}

function timeToString()
{
    return hours + ":" + minutes + ":" + seconds;
}


function draw ()
{
    ctx.clearRect(0,0, canvas.width, canvas.height);
    ctx.fillText(timeToString(), canvas.width/2, canvas.height/2);
}

function secondsToMinutes(seconds)
{
    return parseInt(seconds/60);
}

function minutesToHours(minutes)
{
    return parseInt(minutes/60);
}


function countDown(hours, minutes, seconds)
{
    interval = setInterval(()=>
    {
	seconds--;
	check();
	draw();
    }, 1000);
}

function check ()
{
    if (seconds >)
}

