
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'package)
(add-to-list 'package-archives (cons "melpa-stable" "http://stable.melpa.org/packages/") t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tsdh-dark)))
 '(package-selected-packages (quote (emmet-mode ## slime))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(setq inferior-lisp-program "/usr/pkg/bin/sbcl")
(require 'slime-autoloads)
(setq slime-contribs '(slime-fancy))


;; fuck autosave
;disable backup
(setq backup-inhibited t)
;disable auto save
(setq auto-save-default nil)

(global-set-key (kbd "C-c h") 'html-mode)
(global-set-key (kbd "C-c p") 'perl-mode)
(global-set-key (kbd "C-c l") 'slime-mode)
(global-set-key (kbd "C-c (") 'electric-pair-mode)


