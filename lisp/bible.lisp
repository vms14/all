(ql:quickload "cl-ppcre")


(defparameter oneline (with-open-file (bible "bible.txt")
			(read-line bible) (read-line bible) (read-line bible) (read-line bible)))

(ppcre:register-groups-bind (chapter verse text) ("(\\d):(\\d) ([\\w ]*)" oneline) (list chapter verse text))

