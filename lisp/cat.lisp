(load "/home/vms/lisp/vms.lisp")
(defun cat (&rest files)
  "Returns a list with the content of the files"
  (loop for file in files
       collect (read-file file)))
