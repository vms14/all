(ql:quickload "clx")

(defpackage :oh
  (:use :common-lisp :xlib))
(in-package :oh)
(defparameter dis (open-display ""))
(defparameter screen (car (display-roots dis)))
(defparameter root (screen-root screen))
(defparameter win nil)
(defparameter gc (create-gc)
  (defun create-win (&key (w 300) (h 300) (x 0) (y 0))
    (create-window :parent root
		   :x x
		   :y y
		   :width w
		   :height h
		   :event-mask (make-event-mask
				:exposure)))

(setf win (create-win))
(map-window win)
(display-finish-output dis)
(destroy-window win)
(event-case (dis :force-output-p t :discard-p t)
  (:exposure
   ()
   (format t "oh~%")))
