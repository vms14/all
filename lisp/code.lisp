(defmacro sexp-lambda-list (&rest quoted-code)
  "Returns a list of lambdas from a list of sexp. Example (code-lambda-list (+ 1 2 3) (car list))"
  `(list ,@(loop for code in quoted-code
	      collect `(lambda () ,code))))
