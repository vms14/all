(load "/home/vms/lisp/vms")
(load "/home/vms/lisp/html")

(defun gallery-css ()
  (css
   '("@font-face"
     font-family gallery
     src "url(gallery.ttf)")
   '(":root,body"
     width 100%
     height 100%
     margin 0
     padding 0)
   '(.gallery
     display flex
     flex-wrap wrap
     width 100%
     height 100%)
   '(.image
     font-family gallery
     transition ".5s box-shadow, 1.5s filter"
     background-size cover
     background-position center
     position relative
     margin 0
     flex 1
     filter "grayscale(24%)"
     min-width 30vw
     min-height 24vh
     display flex
     align-items flex-end
     justify-content space-around
     color white)
   '(".image div,.gallery-display div"
     position absolute
     bottom 0
     left 0
     margin 0
     background "rgba(0,0,0,0.3)"
     width 100%)
   '(".image h5, .image p"
     margin 3px
     font-size 50%
     text-align center
     font-family gallery)
   '(".image h5"
     font-size 50%)
   '(".image:hover"
     z-index 1
     filter none
     box-shadow "0 0 20px black")
   '(".gallery-display"
     color white
     text-shadow "0 0 3ox red"
     position fixed
     width 100%
     z-index 1
     height 100%
     background-size "100% 100%"
     background-repeat no-repeat
     top 0
     left 0)
   '(".gallery-display div"
     text-align center
     font-size 1.5em
     padding-top 7px
     transition "height 1s"
     font-family gallery)
   '(".gallery-display p"
     font-family gallery
     margin 0
     margin-bottom 15px)
   '(".gallery-display h5"
     font-size 1em
     margin 3px)
    '(.remove-button
     position fixed
     padding 15px
     background "rgba(0,0,0,0.3)"
     top 5%
     right 5%
     z-index 2
     color grey
     font-weight bolder
     font-size 1.5em
     box-shadow "0 0 1px black,inset 0 0 1px white")
   '(".remove-button:hover"
     box-shadow "0 0 1px black,inset 0 0 3px white"
     color white)
   '(.arrow-buttons
     position fixed
     bottom 18px
     right 5%
     z-index 2
     font-size 3em
     font-weight bolder
     color grey)
   '(".arrow-buttons span"
     margin "0 5px")
   '(".arrow-buttons span:hover"
     color white)
   '(.white
     color white)
   '(.hidden
     display none)))


(defun create-gallery (&rest images)
  (let ((images-number 0))
    (section :class "gallery"
	     (script (format nil "imagesNumber = ~a;" (length images)))
	     (script :src "js/gallery.js")
	     (style
	      (gallery-css))
	     (css-file "https://fonts.googleapis.com/css?family=Open+Sans&display=swap")
	     (format nil "~{~a~}"
		     (mapcar (lambda (image)
			       (div :class "image" :onclick "imageDisplay(this);" :data-number (incf images-number) :style (format nil "background-image:url(~a);" (car image)) (div (h5 (cadr image)) (p (caddr image)))))
			     images)))))
