(load "vms")
(load "perl")

(defparameter *products* nil)

(defun add-product (name description price image)
  (let ((product (gensym "product")))
    (setp product 'name name)
    (setp product 'description description)
    (setp product 'price price)
    (setp product 'image (format nil "/images?~a" image))
    (push product *products*)))

(defun render-product (product)
  (format nil
	  "<article>~%<h3>~a</h3><section><p>~a</p><i>price: ~a</i><img src=\"~a\"></section></article>"
	  (get product 'name)
	  (get product 'description)
	  (get product 'price)
	  (get product 'image)))
