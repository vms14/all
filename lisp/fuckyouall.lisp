(defparameter numbers (list 1 3 7))

(defun max-number (nums)
  (eval `(max ,@nums)))

(defparameter max-num (max-number numbers))

(defparameter sorted-numbers (sort numbers '>))

(defparameter counter max-num)

(defparameter printable-numbers (list max-num))

(defparameter printed-numbers nil)

(defun printed-p (num)
  (member num printed-numbers))

(defun have-to-print (num)
  (when (member counter numbers)
    (push num printed-numbers)
    t))

(defun numbers-waiting (number-list)
  (if (have-to-print (car number-list))
      t
      (if (printed-p (car number-list))
	  t
	  (if (null (cdr number-list))
	      nil
	      (numbers-waiting (cdr number-list))))))

(defun print-numbers (num)
  (if (have-to-print num)
      (format t "~a" num)
      (if (printed-p num)
	  (format t "#")
	  (when (numbers-waiting num)
	    (format t " ")))))

(print-numbers numbers)
(decf counter)
(print-numbers numbers)
(decf counter)
(print-numbers numbers)
(decf counter)
(print-numbers numbers)


