(setf *print-case* :downcase)
(defun setp (symbol property value)
  (setf (get symbol property) value))

(defun make-text-buffer () (make-array 0 :adjustable t :fill-pointer t :element-type 'character))

(defun create-tag (tag-name)
  (let ((tag (gensym (format nil "~a" tag-name))))
    (setp tag 'name tag-name)
    tag))


;; colors
(defparameter *css-colors* (make-text-buffer))
(defparameter *colors* nil)
(defun set-color (tag color)
  (setp tag :color color))
(defun get-color (tag)
  (get tag :color))
(defun add-color-class (color tag)
  (unless (member color *colors*)
    (format *css-colors* ".~A~%{~%color:~a;~%}~%"color color)
    (push color *colors*))
  (set-color tag color))


(defun render-tag (tag)
  (format t "<~a ")
  (if (get tag 'class)
      (format t ))

