(defun make-text-buffer () (make-array 0 :adjustable t :fill-pointer t :element-type 'character))

(setf *print-case* :downcase)


(defmacro autoclose-tag (tag-name)
  `(defun ,tag-name (&rest attribute-value-lists)
     (let ((attrs)))
     (format nil ,(format nil "<~a~~{~~{~~^ ~~a=\"~~a\"~~}~~}>" tag-name) attribute-value-lists)))

(defmacro tag-generator (tag-name &key autoclose-p)
  (let ((format-string (if autoclose-p
			   (format nil "<~a~~a~~a>" tag-name)
			   (format nil "<~a~~a>~~a</~:*~a>" tag-name))))
    `(defun ,tag-name (&rest stuff)
       (let ((attrs (make-text-buffer))
	     (vals (make-text-buffer)))
	 (labels ((recursion (args)
		    (when (car args)
		      (typecase (car args)
			(keyword
			 (case (car args)
			   (:itemtype
			    (format attrs " itemscope itemtype=\"http://schema.org/~a\"" (cadr args))
			    (recursion (cddr args)))
			   (:itemprop
			    (format attrs " itemprop=\"~a\"" (cadr args))
			    (recursion (cddr args)))
			   (t
			    (format attrs " ~a=\"~a\"" (car args) (cadr args))
			    (recursion (cddr args)))))
			(t
			 (format vals "~a" (car args))
			 (when (cdr args)
			   (recursion (cdr args))))))))
	   (recursion stuff))
	 (format nil ,format-string attrs vals)))))


(defmacro create-tags (&rest tags)
  `(progn ,@(loop for tag in tags
		 collect `(tag-generator ,tag))))
;; no-defun-allowed helped me with this macro
; Saturday, October 12, 2019


(defmacro create-autoclose-tags (&rest tags)
  `(progn ,@(loop for tag in tags
		 collect `(tag-generator ,tag :autoclose-p ,t))))

(create-tags html body head style header main article section ul ol li
 table tr th tbody tfoot footer nav script noscript audio video title
 h1 h2 h3 h4 h5 h6 div span a p style figcaption figure form cite q blockquote script noscript aside textarea label)

(create-autoclose-tags br link img meta input)

(defun css-file (filename)
  (link :href filename :rel "stylesheet" :type "text/css"))

(defun js-file (filename)
  (script :type "text/javascript" :src filename))

(defun named-input (name type input-name)
  (concatenate
   'string
   (label name)
   (input :type type :name input-name)
   (br)))
;;(defun meta (name content)
;;  (format nil "<meta name=\"~a\" content\"~a\">" name content))

(defun viewport ()
  (meta :name "viewport" :content "width=device-width, initial-scale=1"))

(defun css (&rest css-list)
  "Expects (css '(h1 :color red))"
  (let ((buffer (make-text-buffer)))
    (dolist (list css-list)
      (format buffer "~a{~{~a:~a;~}}" (car list) (cdr list)))
    buffer))
(defun html (&rest args)
  (format t "<!DOCTYPE html><html lang=\"en\">~{~a~}</html>" args))
