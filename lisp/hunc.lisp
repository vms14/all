(ql:quickload "hunchentoot")

(hunchentoot:define-easy-handler (oh :uri "/") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "sucio"))
(hunchentoot:define-easy-handler (oh :uri "/") (sucio)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "~a es un sucio" sucio))

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 2424))
