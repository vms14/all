(load "vms")
(load "perl")

(ql:quickload "hunchentoot")

(tbnl:define-easy-handler (img :uri "/img") (text font color border-color)
  (setf (tbnl:content-type*) "image/png")
  (perl :perl-script "/home/vms/perl/image.pl" :arguments (remove-if #'null (list text font color border-color)) :binary-output t :output-as-list t))

(tbnl:define-easy-handler (donate-img :uri "/donate.jpg") ()
  (setf (tbnl:content-type*) "image/jpg")
  (read-binary-file "/home/vms/Downloads/donate.jpg"))

(tbnl:define-easy-handler (donate :uri "/donate") ()
  (setf (tbnl:content-type*) "text/html")
  (read-file "/home/vms/html/donate.html"))

(tbnl:define-easy-handler (home :uri "/") ()
  (setf (tbnl:content-type*) "text/html")
  (format nil "~a~a~%</body>~%</html>" (read-file "/home/vms/html/image.html") (render-fonts-input)))

(defun render-fonts-input ()
  (let ((buffer (make-text-buffer)))
    (format buffer "<select onchange=\"oh(document.body.children[0].children[0]);\">~%")
    (format buffer "~{<option> ~a </option>~%~}</select>~%" (read-file "/home/vms/fonts" :output-as-list-p t))
    buffer))

    
(tbnl:start (make-instance 'tbnl:easy-acceptor :port 2424))
