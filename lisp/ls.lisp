(defun ls (&optional (path "*"))
  (format nil "~{~a~%~}" (directory (make-path path)))
