(ql:quickload "hunchentoot")

(defmacro route (url (&key (type "text/html") parameters) &body content)
  `(tbnl:define-easy-handler (,(gensym) :uri ,url) (,@parameters)
	  (setf (tbnl:content-type*) ,type)
	  ,@content))


(defun start (&key (port 2424) (documents "/home/vms/"))
  (tbnl:start (make-instance 'tbnl:easy-acceptor :port port :document-root documents)))

(defmacro oh (url (&key (type "text/html") parameters) &body content)
  `(list ,url ,type ,parameters ,@content))

(defmacro crear-funcion (nombre-sucio argumento1 &rest code)
  `(defun ,nombre-sucio (,argumento1)
     ,@code))
