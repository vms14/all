(defun flatten (x)
  (labels ((rec (x acc)
	     (typecase x
	       (null acc)
	       (atom  (cons x acc))
	       (list (rec (car x) (rec (cdr x) acc))))))
    (rec x nil)))
