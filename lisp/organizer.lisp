#!/usr/pkg/bin/sbcl --script

(defun read-args ()
  sb-ext:*posix-argv*)

(defconstant +organized-directory+
  (or
   (cadr (read-args))
   (user-homedir-pathname)))

(format t "~a" +organized-directory+)
