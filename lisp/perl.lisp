(load "vms")

(defparameter *perl-location* "/usr/pkg/bin/perl")

(defun perl (&key perl-script arguments files modules input binary-output output-as-list)
  (let ((argument-list nil))
    (dolist (file files)
      (push file argument-list))
    (dolist (argument (reverse arguments))
      (push argument argument-list))
    (unless (null modules)
      (if (listp modules)
	  (dolist (module modules)
	    (push (format nil "-M~a" module) argument-list))
	  (push (format nil "-M~a" modules) argument-list)))
    (push perl-script argument-list)
    (exec *perl-location* :arguments (remove-if #'null argument-list) :input input :binary-output binary-output :output-as-list output-as-list)))

(defun oneliner (line &key (arguments "-e") files modules input binary-output output-as-list)
  (perl :arguments (list arguments line) :files files :modules modules :input input :binary-output binary-output :output-as-list output-as-list))

