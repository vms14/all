(defun make-text-buffer () (make-array 0 :element-type 'character :fill-pointer t :adjustable t))
(defparameter json-ld (make-text-buffer))

(defparameter *indentation* 0)
(defmacro fmt (buffer string &rest args)
  `(format ,buffer ,(format nil "~vt~a" *indentation* string) ,@args))

(defun setp (symbol property value)
  (setf (get symbol property) value))
(defmacro with-indentation (level &body body)
  `(progn
     (incf *indentation* ,level)
     ,@body
     (decf *indentation* ,level)))
(defun json (name-value)
  (format t "~a;" (car name-value))
  (typecase (cadr name-value)
    (list
     (fmt t "[~%~t")
     (json (cadr name-value))
     (fmt t "~%]~%"))
    (symbol
     (fmt t "~a" (cadr name-value)))))
(defun name-value (name value)
  (list name value))



(defun name-value-list (&rest lists)
  (let ((buffer (make-text-buffer)))
    (flet ((write-value-list (text-buffer value-list)
	     (fmt text-buffer "" (car value-list)))))
      (ftm buffer "~1t"))

(defun context ()
  (name-value "@context" "http://schema.org"))

(defun schema-type (type-name)
  (name-value "@type" type-name))

(defun person-microdata ()
  (format json-ld "{~%")
  
  


