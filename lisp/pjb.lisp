(let ((indices (list 0)))
  (defun cl-user::fmt-index (stream arg colon at &rest parameters)
    "
ARG is ignored: pass NIL or use ~:* before or after to eat previous or
next argument and regurgitate.

Set current index to 42:     ~,42@/fmt-index/
Pop indices:                 ~0@/fmt-index/
push 42 onto indices:        ~1,42@/fmt-index/
clear indices stack:         ~2@/fmt-index/

format index (and increment):

   ~V,V,V,V,V/fmt-index/  width delim mode from-ch to-ch

    mode
    nil   normal integer formating ~D
    0:    ~R   (four)
    1:    ~:R  (fourth)
    2:    ~@R  (IV)
    3:    ~:@R (IIII)
    4:    use letters between the two given in the following parameters inclusive,
          or by default, lowercase letters a b # z aa ab # zz #
"
    (declare (ignore arg))
    (if at
        (destructuring-bind (&optional op (start 0) &rest ignored) parameters
          (declare (ignore ignored))
          (check-type op    (or null (integer 0 2)))
          (check-type start integer)
          (case op
            ((nil) (setf (first indices) start))
            ((0)   (pop indices))
            ((1)   (push start indices))
            ((2)   (setf indices (list start)))))
        (destructuring-bind (&optional width delim mode (from #\a) (to #\z) &rest ignored)
            parameters
          (declare (ignore ignored))
          (check-type width (or null integer))
          (check-type delim (or null character))
          (check-type mode  (or null (integer 0 4)))
          (format stream "~V<~A~>" (or width 0)
                  (format nil (case mode
                                ((nil)    "~D")
                                ((0)      "~R")
                                ((1)      "~:R")
                                ((2)      "~@R")
                                ((3)      "~@:R")
                                ((4)      "~A"))
                          (if (eql mode 4)
                              (encode-index (first indices) from to)
                              (first indices))))
          (format stream "~@[~A~]" delim)
          (incf (first indices))))))
(defun cardinal-to-base (n base)
  (check-type n    (integer 0))
  (check-type base (integer 2))
  (if (zerop n)
      (list 0)
      (loop
        :while (plusp n)
        :collect (mod n base)
        :do (setf n (truncate n base)))))

(defun encode-index (index from to)
  (let* ((letters        (coerce (loop :for code :from (char-code from) :to (char-code to)
                                       :collect (code-char code)) 'vector))
         (base           (length letters))
         (digits         (cardinal-to-base (1- index) base))
         (representation (make-string (length digits))))
    (loop
      :for i :from (1- (length representation)) :downto 0
      :for digit :in digits
      :do (setf (aref representation i) (aref letters digit)))
    representation))
