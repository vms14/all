(defun setp (symbol property value)
  (setf (get symbol property) value))
(defun make-product (name description price)
  (let ((product (gensym "product")))
    (setp product :name name)
    (setp product :description description)
    (setp product :price price)
    product))

(defmacro check-and-setp (&rest lists)
  `(progn
     ,@(loop for list in lists
	  collect `(when ,(car list)
		     (setp ,@(cdr list))))))

(defun make-user (name &key phone mail comments bonus registration-date)
  (let ((user (gensym "user")))
    (setp user :name name)
    (check-and-setp
     (phone user :phone phone)
     (mail user :mail mail)
     (comments user :comments comments)
     (bonus user :bonus bonus))
    (setp user :registration-date (or registration-date (set-date)))
    user))

(defun set-date ()
  (format nil "today"))
(defparameter *users* nil)
(defparameter *products* nil)
(defun add-user (user)
  (push user *users*))


(defun user-bought-product (user product)
  (push product (get user :products)))
