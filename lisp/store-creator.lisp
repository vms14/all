(load "/home/vms/lisp/html.lisp")

(defun product (name image-url price description product-url)
  (article :itemtype "Product"
	   (h3 :itemprop "name" name)
	   (figure
	    (img :itemprop "image" :src image-url)
	    (figcaption
	     (p :itemprop "description" description)
	     (meta :itemprop "url" :content product-url)
	     (span :itemprop "aggregateRating" :itemtype "AggregateRating"
		   (meta :itemprop "ratingValue" :content "87")
		   (meta :itemprop "bestRating" :content "100")
		   (meta :itemprop "ratingCount" :content "24"))
	     (span :itemprop "brand" :itemtype "Brand"
		   (meta :itemprop "name" :content "Plantas aromaticas"))
	     (meta :itemprop "sku" :content "oh")
	     (meta :itemprop "mpn" :content "oh")
	     (span :itemprop "review" :itemtype "Review"
		   (meta :itemprop "name" :content "oh")
		   (meta :itemprop "author" :content "oh")
		   (span :itemprop "reviewRating" :itemtype "Rating"
			 (meta :itemprop "ratingValue" :content 1)))
	     (span :itemprop "offers" :itemtype "Offer"
		   (span :itemprop "price" price)
		   (meta :itemprop "url" :content product-url)
		   (link :itemprop "availability" :href "http://schema.org/InStock")
		   (meta :itemprop "priceValidUntil" :content "3000-01-01")
		   (span :itemprop "priceCurrency" "EUR"))))))
