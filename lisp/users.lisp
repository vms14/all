(defun ordered-list (list)
  (dotimes (i (length list))
    (format t "~a. ~a~%" (1+ i) (nth i list)))
  list)
(defun show-menu (options-list functions-list)
  (ordered-list options-list)
  (funcall (nth (1- (read)) functions-list)))

(defmacro when-answer (what-to-ask &body code)
  (format t "~a~%" what-to-ask)
  `(when )(read-line))

(defmacro sexp-lambda-list (&rest quoted-code)  
  `(list ,@(loop for code in quoted-code
	      collect `(lambda () ,code))))
;; Bike gave me this macro 
;; Thanks man <3
; Tuesday, October 8, 2019

(defun yes-no-menu (options-list functions-list)
  (dotimes (i (length options-list))
    (when (yes-or-no-p (format nil "~a? " (nth i options-list)))
	(funcall (nth i functions-list)))))

(defun add-user (user-name &keyword user-home-directory (account-expire-date "0"))
  
(show-menu '("Add/remove users and manage passwords"
	     "Create/remove groups"
	     "Modify user group"
	     "Change file owner"
	     "Check existing users and logged users"
	     "Exit")
	   (sexp-lambda-list
	    ;; Option 1 Add or remove users and manage passwords
	    (show-menu '("Add user"
			 "Remove users")
		       (sexp-lambda-list
			;; Add user
			(progn
			  (ask "User-name"))))
	    (progn
	      (format t "You choose the option 2, Create or remove groups~%"))))


