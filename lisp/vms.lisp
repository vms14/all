(defun make-text-buffer ()
  (make-array 0 :adjustable t :fill-pointer t :element-type 'character))

(defun read-lines (stream)
  (loop for line = (read-line stream nil)
     while line
     collect line))

(defun read-bytes (stream)
  (loop for byte = (read-byte stream nil)
     while byte
     collect byte))

(defun read-binary-file (filename)
  (with-open-file (buffer filename :element-type '(unsigned-byte 8))
    (read-bytes buffer)))

(defun read-file (filename &key output-as-list-p)
  (with-open-file (buffer filename)
    (if output-as-list-p
	(read-lines buffer)
	(format nil "~{~a~^~%~}" (read-lines buffer)))))

(defun read-data (filename)
  (read-from-string (read-file filename)))

(defun exec (executable &key arguments input binary-output output-as-list)
  (let* ((process (sb-ext:run-program executable arguments :wait nil :input :stream :output :stream))
	 (process-input (sb-ext:process-input process))
	 (process-output (sb-ext:process-output process)))
    (when input
      (write-string input process-input)
      (close process-input))
    (let ((output (if binary-output
		      (read-bytes process-output)
		      (read-lines process-output))))
      (close process-output)
      (if output-as-list
	  output
	  (format nil "~{~a~^~%~}" output)))))

(defun setp (symbol property value)
  (setf (get symbol property) value))

