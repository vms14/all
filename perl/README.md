# perl

for cpan in NetBSD:
.shrc
PATH="/usr/pkg/lib/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/usr/pkg/lib/perl5/5.30.0${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/usr/pkg/lib/perl5/5.30.0${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/usr/pkg/lib/perl5/bin\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/usr/pkg/lib/perl5/bin/"; export PERL_MM_OPT;


