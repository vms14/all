use strict;
print oh();
sub oh(){"meh\n"}
print oh();
sub oh(){"oh\n"}
print oh();
sub oh(){"wtf\n"}
# If the constant function isn't defined, will work like with normal functions.
# If you redefine normal functions, they'll use the last definition.
# So a constant function when called not being defined yet will have the last
# definition, and once defined will have the current definition.
# You receive warnings once you redefine constant functions.
# The output of this program is:
# wtf
# meh
# oh

