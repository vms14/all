package Car;

sub new
{
    $car = {};
    bless $car, shift;
    $car->{passengers} = {};
    $car;
}

sub enter
{
    $car = shift;
    $car->{passengers}->{$_} = "oh" for(@_);
    $car;
}

1;
