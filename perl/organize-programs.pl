use File::Copy;

=head1 NAME

Organize-programs
=cut
=head1 DESCRIPTION

A little and dirty script to organize files in folders.

Searches for file extensions using simple regexp and creates a folder for the language if not exists.

This script will overwrite existing files with the same name inside those folders.

TO-DO list:

 - Add more languages

 - Rename the program and make it organize all kinds of files

 - Manage web projects, I'm skipping html and css (they're not programming languages) because usually they go in a project folder, but this script moves js files to a js folder.

 - Rewrite this shit entirely and make some sort of program/project manager. 

If you have some hints to improve it, some languages to add, or whatever that could help, just say it.

My code has no license unless it inherits license from the language or some module/library/whatever.

You're encouraged to take my shit and do whatever you want with it.

I'm not a very organized person, but this little script helps a bit in maintaining order on your home folder. 

I usually end with a lot of different scripts and tests and my home folder is a mess.

Maybe your home folder is full of random stuff too.

Even if you don't know perl, it won't cost too much to understand and modify this script.

The script is very simple, looks for files in the current directory and uses regular expressions.

If some regexp matches, it takes the current file ($_) and moves it in his folder.

If a folder does not exist it creates one. 

This is what 'mkdir "lang" unless (-d "lang")' does.

Hope it serves for something or it evolves to be more useful.

Even if the code and the program are shit, it could give you a new idea for making a program and you'll end with a better program than this one made in whatever language you wanted.


=cut

for (glob "*")
{
    if (/.*(\.pl)$/)
    {
	mkdir "perl" unless (-d "perl");
	move ($_, "perl");
    }

    if (/.*(\.js)$/)
    {
	mkdir "js" unless (-d "js");
	move ($_, "js");
    }

    if (/.*(\.lisp)$/)
    {
	mkdir "lisp" unless (-d "lisp");
	move ($_, "lisp");
    }

<<<<<<< HEAD
    if (/.*(\.c)$/)
=======
    if (/.*(\.c)$/ || /.*(\.h)/)
>>>>>>> 	new file:   .#mail.pl
    {
	mkdir "c" unless (-d "c");
	move ($_, "c");
    }

<<<<<<< HEAD
    if (/.*(\.cpp)$/)
=======
    if (/.*(\.cpp)$/ || /.*(\.hpp)$/)
>>>>>>> 	new file:   .#mail.pl
    {
	mkdir "cpp" unless (-d "cpp");
	move ($_, "cpp");
    }
    
    if (/.*(\.py)$/)
    {
	mkdir "python" unless (-d "python");
	move ($_, "python");
    }

    if (/.*(\.asm)$/)
    {
	mkdir "asm" unless (-d "asm");
	move ($_, "asm");
    }

    if (/.*(\.lua)$/)
    {
	mkdir "lua" unless (-d "lua");
	move ($_, "lua");
    }

    if (/.*(\.rb)$/)
    {
	mkdir "ruby" unless (-d "ruby");
	move ($_, "ruby");
    }

    if (/.*(\.rs)$/)
    {
	mkdir "rust" unless (-d "rust");
	move ($_, "rust");
    }

    if (/.*(\.sh)$/ || /.*(\.bash)$/)
    {
	mkdir "sh" unless (-d "sh");
	move ($_, "sh");
    }

    if (/.*(\.csh)$/)
    {
	mkdir "csh" unless (-d "csh");
	move ($_, "csh");
    }

    if (/.*(\.zsh)$/)
    {
	mkdir "zsh" unless (-d "zsh");
	move ($_, "zsh");
    }
    
    if (/.*(\.tcsh)$/)
    {
	mkdir "tcsh" unless (-d "tcsh");
	move ($_, "tcsh");
    }
    
    if (/.*(\.scm)$/ || /.*(\.sps)$/ || /.*(\.sls)$/ || /.*(\.sld)$/ || /.*(\.sc)$/ || /.*(\.ss)$/)
    {
	mkdir "scheme" unless (-d "scheme");
	move ($_, "scheme");
    }
    
    if (/.*(\.rkt)$/ || /.*(\.rkts)$/ || /.*(\.rktl)$/)
    {
	mkdir "racket" unless (-d "racket");
	move ($_, "racket");
    }
    
    if (/.*(\.awk)$/)
    {
	mkdir "awk" unless (-d "awk");
	move ($_, "awk");
    }
    
    if (/.*(\.java)$/)
    {
	mkdir "java" unless (-d "java");
	move ($_, "java");
    }
    
    if (/.*(\.hs)$/)
    {
	mkdir "haskell" unless (-d "haskell");
	move ($_, "haskell");
    }
    
    
}

