#TODO
# add "include/config" file
# mutate extensions.
# example:
# .pmm to be moved on perl/special-modules/ instead of perl/modules,
# changing .pmm to .pm
# perl/special-modules .pmm changes to .pm
# see ./parser.pl

use strict;
use File::Copy;
use Path::Tiny;

my $user_path = shift || glob ("~");

my %folders_and_extensions =
(

 # Programming and markup languages
 'perl' => [qw/pl/],
 'perl/modules' => [qw/pm/],
 'c' => [qw/c/],
 'c/include' => [qw/h/],
 'cpp' => [qw/cpp/],
 'cpp/include' => [qw/hpp/],
 'lisp' => [qw/lisp/],
 'markdown' => [qw/md/],
 'html' => [qw/html/],
 'css' => [qw/css/],
 'js' => [qw/js/],
 'php' => [qw/php/],

 # Database formats
 'database' => [qw/db/],
 
 # Serialization formats
 'json' => [qw/json/],
 'xml' => [qw/xml/],
 
 # Document types
 'text' => [qw/txt/],
 'documents' => [qw/doc/],
 'images/png' => [qw/png/],
 'images/bmp' => [qw/bmp/],
 'images/jpg' => [qw/jpg/],
 'pdf' => [qw/pdf/],
 'music' => [qw/mp3 wav ogg/],


 # Personal stuff

 # Test
 'oh' => [qw/oh/],
);

for my $file (glob "*")
{
	 for my $folder (keys %folders_and_extensions)
	 {
		  my $extension = $folders_and_extensions{$folder};
		  for(@$extension)
		  {
				if ($file =~ m/\.$_$/)
				{
					 my $path = Path::Tiny::path($user_path, $folder);
					 $path->mkpath();
					 File::Copy::move ($file, $path);
				}
		  }
	 }
}
