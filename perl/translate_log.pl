use strict;
use warnings;

my @days = qw/oh Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec/;

while (<>)
{
    next if ($_ =~ /^#/);
    chomp $_;
    chop $_;
    my @l = split " ", $_;
    my @date = split "-", $l[0];
    $date[1] = $days[$date[1]];
    @date = reverse @date;
    my $date = join "/", @date;
    print "$l[8] - - [$date:$l[1] +0000] \"$l[3] $l[4] HTTP/1.1\" $l[-4] $l[-1]\n";
    
}
