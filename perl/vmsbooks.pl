use HTTP::Tiny;
use warnings;

sub take_links
{
    return split "\n", HTTP::Tiny->new->get("https://gitlab.com/vms14/perl/raw/master/links")->{content};
}

sub gitlab_books
{
    my @return_arrau;
    for(split "\n", HTTP::Tiny->new->get(shift)->{content})
    {
	push @return_array, "$1\n" if /href=\"(.*\.pdf)\">/;
    }
    return @return_array;

}

sub github_books
{
    my @return_arrau;
    for(split "\n", HTTP::Tiny->new->get(shift)->{content})
    {
	push @return_array, "$1\n" if /href=\"(.*\.pdf)\">/;
    }
    return @return_array;

}

sub gitlab_links
{
    my @return_array;
    
    for(shift)
    {
	if ($_ =~ m/gitlab/)
	{
	    push @return_array, "$1\n" if /href=\"(.*\.pdf)\">/;
	}
    }
    return @return_array;
}

sub github_links
{
    my @return_array;
    
    for(shift)
    {
	push (@return_array, "$_") if /github/;
    }
    return @return_array;
}

github_links take_links;
print [take_links]->[0];
$meh = (take_links())[0];
print "\n$meh\n";

